import React from "react";
import { StyleSheet, View, Image } from "react-native";
import colors from "../config/colors";
import Icon from "../components/Icon";
function ViewImageScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.closeIcon}>
        <Icon name="close" iconColor="white" size={40} />
      </View>
      <View style={styles.deleteIcon}>
        <Icon name="trash-can-outline" size={40} iconColor={colors.primary} />
      </View>

      <Image
        resizeMode="contain"
        style={styles.image}
        source={require("../assets/chair.jpg")}
      ></Image>
    </View>
  );
}

const styles = StyleSheet.create({
  closeIcon: {
    position: "absolute",
    top: 15,
    left: 10,
  },

  deleteIcon: {
    position: "absolute",
    top: 15,
    right: 10,
  },
  image: {
    width: "100%",
    height: "100%",
  },
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
});
export default ViewImageScreen;
