import React from "react";
import LottieView from "lottie-react-native";
import { View, StyleSheet } from "react-native";
export default function ActivityIndicator({ visible = false }) {
  if (!visible) return null;
  return (
    <View style={styles.overlay}>
      <LottieView
        autoPlay
        loop
        source={require("../assets/animations/lf30_editor_NsArrq.json")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  overlay: {
    position: "absolute",
    height: "100%",
    width: "100%",
    backgroundColor: "white",
    zIndex: 1,
    opacity: 0.8,
  },
});
