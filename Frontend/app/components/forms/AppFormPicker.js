import React from "react";
import { StyleSheet } from "react-native";
import { useFormikContext } from "formik";

import AppPicker from "../AppPicker";
import ErrorMessage from "./ErrorMessage";

export default function AppFormPicker({
  items,
  name,
  numOfColumns,
  PickerItemComponent,
  width,
  placeholder,
}) {
  const { errors, setFieldValue, touched, values } = useFormikContext();
  return (
    <>
      <AppPicker
        items={items}
        onSelectItem={(item) => setFieldValue(name, item)}
        PickerItemComponent={PickerItemComponent}
        placeholder={placeholder}
        numOfColumns={numOfColumns}
        selectedItem={values[name]}
        width={width}
      />
      <ErrorMessage err={errors[name]} visible={touched[name]} />
    </>
  );
}

const styles = StyleSheet.create({});
