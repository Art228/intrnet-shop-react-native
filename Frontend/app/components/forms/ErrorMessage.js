import React from "react";
import { StyleSheet } from "react-native";
import AppText from "../AppText";

function ErrorMessage({ err, visible }) {
  if (!visible || !err) return null;

  return <AppText style={styles.error}>{err}</AppText>;
}

const styles = StyleSheet.create({
  error: {
    color: "red",
  },
});

export default ErrorMessage;
