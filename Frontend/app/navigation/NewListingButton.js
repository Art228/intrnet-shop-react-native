import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import colors from "../config/colors";
import Icon from "../components/Icon";

export default function NewListingButton({ onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Icon
          name="plus-circle"
          iconColor={colors.white}
          backgroundColor={colors.primary}
          size={40}
        />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.primary,
    bottom: 20,
    borderRadius: 35,
    borderColor: colors.white,
    borderWidth: 10,
    height: 70,
    width: 70,
  },
});
